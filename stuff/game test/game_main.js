function Game(hostSelectorId, gameUiId, hostWaitingForPlayersId, hostWaitingNumberOfPlayersId, clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId){
    this.host = null;
    this.client = null;
    function unHideUi(){
        document.getElementById(hostSelectorId).style.display="none";
    }
    this.launchGameAsHost = function (hostId){
        unHideUi();
        this.host = new Host(hostWaitingForPlayersId, hostWaitingNumberOfPlayersId, clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId);
        this.host.hostGame(hostId);
        this.client = this.host.ownClient;
    };
    this.launchGameAsClient = function (hostId){
        unHideUi();
        this.client = new Client(clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId);
        this.client.joinGame(hostId);
    };
}

