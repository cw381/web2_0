
function Client(clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId){

    this.state = clientStates.waiting_to_join;
    this.cards = [];
    this.cardsOnTable = [];
    this.gameState = null;
    this.numberOfPlayers = null;
	this.currentround = 0;

    /**
    * Joins a game identified by hostId.
    **/
    this.joinGame = function(hostId){
        this.updateClientJoining();
        

        this.state = clientStates.joining;
        this.hostId = hostId;
        this.clientPeer = new Peer();
        this.clientConn = this.clientPeer.connect(this.hostId);
        this.clientPeer.on('error', (err) => {
            console.log("Client: Peer-Error");
            console.log(err);        

        });
        this.clientConn.on('open', () => {
            this.clientConn.on('data', (data) => {
                this.handleIncomingMessage(data);
            });
            this.clientConn.send({type : ClientMessageType.join_game});
        });

    }
    /**
    * Sends the action "play a card" to the host
    **/
    this.playCard = function(cardIndex){
        if(this.state == clientStates.in_game && this.gameState == clientGameStates.wait_for_moves){
            let cardToSend = this.cards[cardIndex];
            this.cards.splice(cardIndex,1);
            this.clientConn.send( {
                type : ClientMessageType.play_card,
                card : cardToSend,
                cardsOnTable : this.cardsOnTable //later this could be used to check whteher someone else played a card quasi-simultaneously
            });
            //later we might implement waiting for a success message by the host... for now we just remove the played card

            this.updateCardsOnHand();
        }
    }

    /**
    * Updates the gui to show the cards currently on hand.    
    **/
    this.updateCardsOnHand = function() {
        let innerHtml = "";
		//document.cardswidget.dialog( "open" );	// does not work yet
        for(let i=0; i< this.cards.length; i++){
            innerHtml += //"<img src='./images/cards_single.svg' onclick='game.client.playCard(" + i + ")'/><div style='position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'>" + this.cards[i] + "</div>";
"<div class='Column' id='card" + i + "' title='" + this.cards[i] + "'><span style='cursor: pointer;' onclick='game.client.playCard(" + i + ")'><img src='./images/cards_single.svg'/><div style='position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'><span id='CardOnHandTextStyle'>" + this.cards[i] + "</span></div></span></div> ";
        }
        document.getElementById(ownCardsId).innerHTML = innerHtml;
    };

    /**
    * Updates the gui to show cards currently on the table.
    **/
    this.updateCardsOnTable = function() {
        if(this.cardsOnTable.length-1 >= 0){
            document.getElementById(topCardTextId).innerText = "" + this.cardsOnTable[this.cardsOnTable.length-1];
			document.getElementById(topCardTextId).parentElement.style.display = "inline";
			document.getElementById(topCardTextId).parentElement.title = "" + this.cardsOnTable[this.cardsOnTable.length-1];
        }else{
            document.getElementById(topCardTextId).innerText = "";
			document.getElementById(topCardTextId).parentElement.style.display = "none";
			document.getElementById(topCardTextId).parentElement.title = "";
        }
    };

    /**
    * Updates the gui to indicate that the client successfully joined the game.
    **/
    this.updateClientJoinSuccess = function(){
        //document.getElementById(clientJoiningId).style.display = "none";
        //document.getElementById(clientJoinedWaitingId).style.display = "block";  
        systemMessage("Joined!");
    };

    /**
    * Updates the gui to indicate that the game the client had joined now was started by the host.
    **/
    this.updateGameStarted = function(){
        //document.getElementById(clientJoinedWaitingId).style.display = "none";
        systemMessage("Game started!");
    };

    /**
    * Updates the gui to indicate that the client is in the process of joining a game.
    **/
    this.updateClientJoining = function(){
        //document.getElementById(clientJoiningId).style.display = "block";
        systemMessage("Joining...");
    }

        /**
    * Updates the gui to indicate that a round was lost/won.
    **/
    this.updateRoundEnded = function(lostOrWon){
        if(lostOrWon == HostMessageType.won_round){
            systemMessage("Round won!");
			this.currentround++;
        }else{
            systemMessage("Sorry, you lost this round.");
        }
        this.updateCardsOnTable();
    }

    /**
    * Updates GUI so table shows exact amount of players.
    **/
    this.updatePlayersAtTable = function(numberOfPlayers){
        let innerHtml = "";
        for(let i =0; i < numberOfPlayers; i++){
            innerHtml += '<div class="hands-crad-container" style="transform: rotate(calc(180deg + (' + i + ' * 360deg / ' + numberOfPlayers + ')));"><div class="hands"><img src="./images/hand.svg"><img src="./images/hand.svg"></div><div class="cards"><img src="./images/cards1.svg"></div></div>';
        }
        document.getElementById("displayplayers").innerHTML = innerHtml;    
    }

    /**
    * Handles messages that were sent by the host.
    **/
    this.handleIncomingMessage = function(data){
        console.log("Client.handleIncomingMessage: state = " + this.state + " gs= " + this.gameState);
        console.log(data);
        switch(data.type){
            case HostMessageType.join_failure_not_waiting_for_players:
            break;
            case HostMessageType.already_joined:
            break;
            case HostMessageType.join_success:
                console.log("Client: join success");
                if(this.state == clientStates.joining){
                    this.state = clientStates.joined_waiting;
                    
                    this.updateClientJoinSuccess();
                }
            break;
            case HostMessageType.start_game:
                if(this.state == clientStates.joined_waiting){
                    this.updateGameStarted();  

                    this.state = clientStates.in_game;
                    this.gameState = clientGameStates.wait_for_cards;
                    this.numberOfPlayers = data.number_of_players;

                    this.updatePlayersAtTable(this.numberOfPlayers);
                }
            break;
            case HostMessageType.hand_out_cards:
                if(this.state == clientStates.in_game && this.gameState == clientGameStates.wait_for_cards){
                    console.log("Client.hIM: setting card(s) " + data.cards);
                    this.gameState = clientGameStates.wait_for_moves;
                    this.cards = [...data.cards];
					
					document.getElementById('headline').innerHTML = "ROUND " + this.currentround;
                    this.updateCardsOnHand();
                }
            break;
            case HostMessageType.update_table:
                this.cardsOnTable = data.cardsOnTable;
                this.updateCardsOnTable();
            break;
            case HostMessageType.lost_round:
            case HostMessageType.won_round:
                console.log("Client.handleIncomingMessage: Lost or won round - prepare for next round");
                this.gameState = clientGameStates.wait_for_cards;
                this.cards = [];
                this.cardsOnTable = [];
                this.updateRoundEnded(data.type);
            break;
            case HostMessageType.won_game:
                this.state = clientStates.ending_game;
                this.gameState = null;
            break;
            
                
        }
    }
    
}
