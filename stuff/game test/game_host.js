
function array_equal(arr1,arr2){
    if(arr1.length != arr2.length){return false;}
    for(let i=0; i < arr1.length; i++){
        if(arr1[i] != arr2[i]){
            return false;
        }
    }
    return true;
}
function test(){
	
	document.write("<u>hostStates:</u><br><table><tr><td>waiting_to_host:</td><td>1</td></tr><tr><td>setup_hosting:</td><td>2</td></tr><tr><td>waiting_for_players:</td><td>3</td></tr><tr><td>starting_game:</td><td>4</td></tr><tr><td>in_game:</td><td>5</td></tr><tr><td>ending_game:</td><td>6</td></tr></table><br><br><u>gameStates:</u> (Host)<br><table><tr><td>hand_out_cards:</td><td>1</td></tr><tr><td>wait_for_moves:</td><td>2<td></tr><tr><td>round_failed:</td><td>3<td></tr><tr><td>round_passed:</td><td>4</td></tr></table><br><br><u>clientStates:</u><br><table><tr><td>waiting_to_join : </td><td>1</tr><tr><td>joining:</td><td>2</tr><tr><td>joined_waiting:</td><td>3</tr><tr><td>in_game:</td><td>4</tr><tr><td>ending_game:</td><td>5</tr></table><br><br><u>clientGameStates:</u><br><table><tr><td>wait_for_cards:</td><td>1</td></tr><tr><td>wait_for_moves:</td><td>2</td></tr><tr><td>round_failed:</td><td>3</td></tr><tr><td>round_passed:</td><td>4</td></tr></table>");
	
    h = new Host();
    h.hostGame("yoloSwag69");
    c = new Client();
    c1 = new Client();
    c2 = new Client();

    c.joinGame("yoloSwag69");
    c1.joinGame("yoloSwag69");
    c2.joinGame("yoloSwag69");
}
function test_short(){
    h = new Host();
    h.hostGame("yoloSwag69");
    c1 = h.ownClient;
    c2 = new Client();
    c2.joinGame("yoloSwag69");

    var cards = [];
    Object.defineProperty(c1,"cards", {
        get: function(){
            return cards;
        },
        set: function(val){
            cards = val;
        }
    });
}
function test_cards(){
    console.log("h.cards == c.cards ? " + array_equal(h.players[0].cards,c.cards));
    console.log("h.cards == c1.cards ? " + array_equal(h.players[1].cards,c1.cards));
    console.log("h.cards == c2.cards ? " + array_equal(h.players[2].cards,c2.cards));    
    
    console.log("h.cardsOnTable == c.cardsOnTable ? " + array_equal(h.cardsOnTable,c.cardsOnTable));
    console.log("h.cardsOnTable == c1.cardsOnTable ? " + array_equal(h.cardsOnTable,c1.cardsOnTable));
    console.log("h.cardsOnTable == c2.cardsOnTable ? " + array_equal(h.cardsOnTable,c2.cardsOnTable));
}
function Host(hostWaitingForPlayersId, hostWaitingNumberOfPlayersId, clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId){


    
    this.state = hostStates.waiting_to_host;
    this.gameState = null;
    this.players = [];
    this.cardsOnTable = [];
    this.roundNumber = null;
    this.maxNumberOfCards = 100;

    this.ownClient = new Client(clientJoiningId, clientJoinedWaitingId, ownCardsId, topCardTextId);
    
    /**
    * Adds the host's client which runs locally and does not use peer-2-peer stuff.
    **/
    this.addOwnClient = function(){
        let self = this;
        this.players.push({peerId : "HOSTHOSTHOSTHOSTHOSTHOSTHOSTHOSTHOST", connection : {send : function(dat) {self.ownClient.handleIncomingMessage(dat);}}, cards : [], isHost : false});
        this.ownClient.state = clientStates.joined_waiting;
        this.ownClient.clientConn = { send : function(dat) {self.handleIncomingClientMessage({peer: "HOSTHOSTHOSTHOSTHOSTHOSTHOSTHOSTHOST", send : self.ownClient.handleIncomingMessage}, dat);}};
        //this.ownClient.clientConn.send = this.handleIncomingClientMessage;
        console.log("Host.addOwnClient(...): Added *local* player " + this.players.length); //TODO display Player Name 
        
    }

    /**
    * Updates the UI to indicate that players are awaited.    
    **/
    this.updateWaitingForPlayers = function() {
        //document.getElementById(hostWaitingForPlayersId).style.display = "block";
        //document.getElementById(hostWaitingNumberOfPlayersId).innerText = "1";
		newLogMessage("[username] joined");
        systemMessage("Waiting for players...");
    };

    this.updateGameStarted = function () {
        //document.getElementById(hostWaitingForPlayersId).style.display = "none";
    }

    this.updateNumberOfPlayersJoined = function(number){
        //document.getElementById(hostWaitingNumberOfPlayersId).innerText = number;
        systemMessage("Player joined. Number of players now: " + number);
    }

    /**
    * Opens up a game with the specified hostId to be joined by players.
    **/
    this.hostGame = function(hostId){
        if(this.state == hostStates.waiting_to_host){
            this.state = hostStates.setup_hosting;
            this.hostId = hostId;
            this.hostPeer = new Peer(this.hostId,{key: 'lwjd5qra8257b9', id: this.hostId});
            
            this.addOwnClient();

            this.updateWaitingForPlayers();

            this.hostPeer.on('open', (id) => {
                if(this.hostId.localeCompare(id) != 0){
                    console.log("Host.hostGame(...): IDs dont match! id = " + id + " this.hostId = " + this.hostId);
                    return;
                }
                this.state = hostStates.waiting_for_players;
            });
            this.hostPeer.on('connection', (conn) => {
                conn.on('open', ()=> {
                    // Receive messages
                    conn.on('data', (data) => {
                         this.handleIncomingClientMessage(conn, data);
                    });  
                });
            });
        
        }else{
            console.log("Host.hostGame(...): Already hosting a game. this.state = " + this.state);        
        }
    }


    /**
    * Starts a hosted game. Can only be called, when hostGame has
    * been called before.
    **/
    this.startGame = function(){
        if(this.state == hostStates.waiting_for_players){
             this.updateGameStarted();

            this.state = hostStates.starting_game;
            this.players.forEach(player => {
                player.connection.send({type: HostMessageType.start_game, number_of_players: this.players.length});
            });
            this.state = hostStates.in_game; //kind of useless, could be merged into one state, but could be of use later
            this.roundNumber = 1;
            this.gameState = gameStates.hand_out_cards;
            this.startRound(this.roundNumber);
        }
    }

    this.startRound = function(numberOfCards){
        if(this.state == hostStates.in_game && this.gameState == gameStates.hand_out_cards){
            if(this.players.length * numberOfCards > this.maxNumberOfCards){
                console.log("Host.startRound: Not enough cards.");
                return; //not enough cards          
            }
            this.cardsInRound = [];
            this.cardsOnTable = [];
            console.log("Host.startRound: Starting random card generation.");
            for(let i = 0; i < this.players.length * numberOfCards; i++){
                let randomNumber = Math.floor(Math.random() * (this.maxNumberOfCards - 1) + 1);
                while(this.cardsInRound.includes(randomNumber)){
                    randomNumber = Math.floor(Math.random() * (this.maxNumberOfCards - 1) + 1);             //TODO: with this.maxNumberOfCards cards to generate, takes forever, probably because this.maxNumberOfCards can never be reached?
                }
                this.cardsInRound.push(randomNumber);
            }

            console.log("Host.startRound: Finished random card generation.");
            for(let i = 0; i < this.players.length; i++){
                this.players[i].cards = this.cardsInRound.slice(numberOfCards * i,numberOfCards * i + numberOfCards);
				this.players[i].cards.sort(function(a, b){return a-b}); // optioal
                this.players[i].connection.send({type : HostMessageType.hand_out_cards, cards : this.players[i].cards});
            }
            this.gameState = gameStates.wait_for_moves;
        }
    }

    this.handleIncomingClientMessage = function (conn, data){
        switch(data.type){
            case ClientMessageType.join_game:
                if(this.state != hostStates.waiting_for_players){
                    console.log("Host.handleIncomingMessage(...): Client rejected, not waiting for players!");
                    conn.send({type : HostMessageType.join_failure_not_waiting_for_players});
                    break;
                }
                if (this.players.some(e => e.peerId === conn.peer)){
                    console.log("Host.handleIncomingMessage(...): Client has already joined!");
                    conn.send({type : HostMessageType.already_joined});
                } else {
                    this.players.push({peerId : conn.peer, connection : conn, cards : [], isHost : false});
                    console.log("Host.handlingIncomingMessage(...): Added player " + this.players.length); //TODO display Player Name 
                    conn.send({type : HostMessageType.join_success});

                    this.updateNumberOfPlayersJoined(this.players.length);
                }
            break;

            case ClientMessageType.play_card:
                if(this.state == hostStates.in_game && this.gameState == gameStates.wait_for_moves){
                    let playing_players = this.players.filter(player => player.peerId === conn.peer);
                    if(playing_players.length != 1){
                        console.log("Host.handleIncomingMessage: Problem identifying player,playing a card.");
                    }
                    this.playCard(playing_players[0],data.card);
                }
            break;
        }
    }

    

    this.playCard= function(player, cardPlayed){
        console.log("host.playCard: Card played = " + cardPlayed);
        let index = player.cards.indexOf(cardPlayed);
        if(index == -1){
            console.log("Host.playCard: Client does not own card " + cardPlayed);
            return;
        }else{
            player.cards.splice(index,1);
        }
        if(this.players.some(player => 
            player.cards.some(cardOwned => cardOwned < cardPlayed)
        )){
            this.roundLost();
            return;
        }
        this.cardsOnTable.push(cardPlayed);
        this.updatePlayerTables();
        //check if round was won
        if(this.cardsOnTable.length == this.players.length * this.roundNumber){
            this.roundWon();
        }
        
    }

    /***
    * Send messages to player clients notifying them of the won round. Starting
    * the next round, if any rounds left. Notifying clients of won game otherwise.
    ***/
    this.roundWon = function(){
        console.log("Host.roundWon(): Round won!");
        this.players.forEach(player => {
            player.connection.send({type : HostMessageType.won_round});
        });
        this.roundNumber++;
        if(this.players.length * this.roundNumber > this.maxNumberOfCards){
            this.gameWon();
            return;
        }
        this.gameState = gameStates.hand_out_cards;
        this.startRound(this.roundNumber);
        
    }    
    
    /***
    * Send messages to player clients notifying them of the lost round.
    ***/
    this.roundLost = function(){
        console.log("Host.roundLost(): Round lost!");
        this.players.forEach(player => {
            player.connection.send({type : HostMessageType.lost_round});
        });
        this.gameState = gameStates.hand_out_cards;
        this.startRound(this.roundNumber);
    }

    this.gameWon = function(){
        console.log("Host.gameWon(): Game won!");
        this.players.forEach(player => {
            player.connection.send({type : HostMessageType.won_game});
        });
        this.gameState = null;
        this.state = hostStates.ending_game;
    }

    this.updatePlayerTables = function(){
        this.players.forEach(player => {
            player.connection.send({type : HostMessageType.update_table, cardsOnTable : this.cardsOnTable});
        });  
    }
    
}


