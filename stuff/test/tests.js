
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


QUnit.test( "Host and client states test.", function( assert ) {
    client = new Client();
    host = new Host();  
    assert.ok( host.state == hostStates.waiting_to_host, "Host: waiting_to_host - Passed!" );


    host.hostGame("kjsngsjgj")
    assert.ok( host.state == hostStates.setup_hosting, "Host: setup_hosting - Passed!" );


    var done = assert.async();
    
    setTimeout(function(){
        assert.ok( host.state == hostStates.waiting_for_players, "Host: waiting_for_players - Passed!");
        done();
    },10000);

    assert.ok( client.state == clientStates.waiting_to_join, "Client: waiting_to_join - Passed!" );


    client.joinGame("kjsngsjgj");
    assert.ok( client.state == clientStates.joining, "Client: joining - Passed!" );

  /*  done = assert.async();
    
    setTimeout(function(){
        assert.ok( client.state == clientStates.joined_waiting, "Client: joined_waiting - Passed!");
        done();
    },10000);
*/
});



QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});
