ClientMessageType = { join_game : 1,
play_card : 2
}
HostMessageType = { join_success : 1,
    already_joined : 2,
    join_failure_not_waiting_for_players : 3,
    start_game : 4,
    hand_out_cards : 5,
    lost_round : 6,
    update_table : 7,
    won_round : 8,
    won_game : 9
}
    clientStates = {
        waiting_to_join : 1,
        joining : 2,
        joined_waiting : 3,
        in_game : 4,
        ending_game : 5
    }
    clientGameStates = { wait_for_cards : 1,
        wait_for_moves : 2,
        round_failed : 3,
        round_passed : 4
    }

    hostStates = {
                            waiting_to_host : 1,
                            setup_hosting : 2,
                            waiting_for_players : 3,
                            starting_game : 4,
                            in_game : 5,
                            ending_game : 6
                                
    };
    gameStates = { hand_out_cards : 1,
        wait_for_moves : 2,
        round_failed : 3,
        round_passed : 4
    }
