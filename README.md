# Web 2.0 und Soziale Netze - Gruppe 2 - The Mind

## How to play the game

Simply download all files in the `src` folder and open the `index.html` file. Everything else is explained there.

## Group

### Members
Tobias Großmann\
Sina Ghashghaie\
Erfan Matbouei\
Conrad Wulf\
Lucky Saleem

### Communication officer
Tobias Großmann

## Project description

### General idea
Create a simple multiplayer game for the webbrowser, like Jackbox Party Games or scribble.io, without the need of a centralized server (based on peer-to-peer connections)

### Used technologies
Javascript (jQuery, jQueryUI), HTML, CSS, PeerJS, WebRTC

We want to implement a modified version of "The Mind" (Links: [NVS](http://middys.nsv.de/middys/the-mind/), [Wikipedia](https://de.wikipedia.org/wiki/The_Mind)). Every player gets the same HTML file or accesses the same website. One player becomes the host and has to share a new generated sesson ID to the other players, who establish the connection to the host. The host maintains the game logic while the clients display the content and transmit the input data. The implementation contains asynchronous synchronisation, event based actions and some fancy animations. There is only a browser required which is capable of WebRTC with support for reliable data channels.

### Rules of "The Mind"
* [English](http://middys.nsv.de/wp-content/uploads/2018/01/TheMind_GB.pdf)
* [German](http://middys.nsv.de/wp-content/uploads/2018/01/TheMind_D.pdf)

### Our modifications
No special cards, no limited amount of lifes.

### Why did we choose this type of application? (Usecase and Business Aspects)
People like to play games. And playing games involves spending time. This is an advantage which we can use: Taking the CPU power in the backround to mine Bitcoins or Ethereum for earning money or donating money to social projects. We could also use the CPU power to help the COVID-19 researches by folding proteins (like Folding@Home). No matter what it will be, the user will be informed about that in the beginning and has the choice to disable this function. 
There is also a lot of space which can be used for ads.

## What are those folders?

- src: containing actual HTML file and all other required files 
- stuff: everything what got created in process of testing and designing stuff

# Develoment related 

### Development status

- [x] Create graphs to design game engine
- [x] Design Frontend 
- [x] Create PeerJS Basis
- [x] Create HTML Basis
- [x] Create CSS Basis
- [x] Connect Jscript with HTML Page
- [ ] Optimizations and bug fixes
 
### Known bugs and needed features
- implement actual chat functionality
- add message and delay when round is lost / won before next round starts
- PeerJS broker server is pretty slow and creating a session works in only 1 of 5 attempts
- errors from PeerJS have to displayed to guide the player
- user has to be able to minimize the chat window if he has a smaller screen
- numbers on cards are not completely centered
- display remaining cards of all players

### Optional features
- animated card movements
- random placement of multible cards in the middle of the table
- Display big logo / game title in the beginning, remove it when popup is closed